import React, { useState, useEffect } from "react";
import { fetchArticles } from "./api";
import "../App.css";
import ContentHome from "./ContentHome";
import FavoriteIcon from '@mui/icons-material/Favorite';
const ArticleComponent = () => {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [expandedIndex, setExpandedIndex] = useState(null);

  useEffect(() => {
    const fetchArticleData = async () => {
      try {
        const articlesData = await fetchArticles();
        setArticles(articlesData);
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    fetchArticleData();
  }, []);

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;

  const handleReadMoreClick = (index) => {
    setExpandedIndex(expandedIndex === index ? null : index);
  };

  return (
    <div>
      <ContentHome article={expandedIndex !== null ? articles[expandedIndex] : null} />
      <div style={{ width: "68rem", marginLeft: "auto", marginRight: "auto",marginTop:"3rem" }}>
        {articles.map((article, index) => (
          <div className="article" key={index} style={{ alignContent: "center", marginBottom: "1rem", display: expandedIndex !== null && expandedIndex !== index ? 'none' : 'block' }}>
            <div>
              <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <img style={{ borderRadius: "50%", marginRight: "1rem" }} src={article.image} alt={article.author} />
                  <p>{article.author}</p>
                </div>
                <div >
                  <button style={{display:"flex",alignItems:"center"}}>
                  <FavoriteIcon /> {article.favoritesCount}
                  </button>
                </div>
              </div>
              <div>
                <h2>{article.title}</h2>
                <p className={`article-body ${expandedIndex === index ? 'expanded' : ''}`}>
                  {expandedIndex !== index && article.body.substring(0, 250) + "..."}
                </p>
                <button style={{ marginTop: "2rem" }} onClick={() => handleReadMoreClick(index)}>
                  {expandedIndex === index ? "Read Less" : "Read More"}
                </button>
              </div>
              <div style={{ marginTop: "0.5rem", opacity: "0.3" }}>
                <hr />
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ArticleComponent;
