import React from 'react';
import "../App.css";
import { Link } from 'react-router-dom';

export default function Appbar() {
  return (                
    <div> 
      <div className='appbar_container'>
        <h2 style={{color:"rgb(92,184,92)"}}>conduit</h2>
        <div>
          <nav>
            <ul>
              <li><Link to="/login">Sign in</Link></li>
              <li><Link to="/register">Sign Up</Link></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
}
