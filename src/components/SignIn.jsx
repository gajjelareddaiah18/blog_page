import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import '../App.css';

export default function SignIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!email || !password) {
      setError("Email and Password are required.");
      return;
    }

    try {
      const response = await axios.post("https://api.realworld.io/api/users/login", {
        user: { email, password }
      });

      if (response.data && response.data.user) {
        setEmail('');
        setPassword('');
        setError('');
        navigate("/home");
      } else {
        setError("Login failed: Invalid credentials");
        console.error("Login failed:", response.data);
      }
    } catch (err) {
      const errorData = err.response?.data?.errors;
      if (errorData) {
        const formattedError = Object.entries(errorData).map(([key, value]) => `${key}: ${value}`).join(', ');
        setError(formattedError);
      } else {
        setError('An unexpected error occurred during login');
      }
      console.error("An error occurred:", err);
    }
  };

  return (
    <div 
      style={{
        display: "flex",
        textAlign: "center",
        width: "33rem",
        flexDirection: "column",
        margin: "auto",
      }}
    >
      <h1 style={{ marginTop: "2rem" }}>Sign In</h1>
      <Link style={{ marginTop: "1rem" }} to='/register'>Need an account?</Link>
      <form onSubmit={handleSubmit}>
        <div 
          style={{
            display: "flex",
            flexDirection: "column",
            marginTop: "1rem"
          }}
        >
          <input
            style={{
              height: "3rem",
              marginTop: "1rem"
            }}
            type="email"
            placeholder="Email"
            value={email}
            onChange={handleEmailChange}
          />
          <input
            style={{
              height: "3rem",
              marginTop: "1rem"
            }}
            type="password"
            placeholder="Password"
            value={password}
            onChange={handlePasswordChange}
          />
          <button
            style={{
              width: "6rem",
              height: "2rem",
              marginTop: "1rem",
              alignSelf: "center",
              marginLeft:"27rem"
            }}
            type="submit"
          >
            Sign in
          </button>
          {error && <p style={{ color: "red", marginTop: "1rem" }}>{error}</p>}
        </div>
      </form>
    </div>
  );
}
