import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

export default function SignUp() {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios({
        method: 'POST',
        url: "https://api.realworld.io/api/users",
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          user: { username, email, password },
        },
      });
      console.log("Registration successful:", response.data);
      setUsername('');
      setEmail('');
      setPassword('');
      navigate('/login');
    } catch (err) {
      const errorData = err.response?.data?.errors;
      const formattedError = errorData ? Object.entries(errorData).map(([key, value]) => `${key}: ${value}`).join(', ') : 'Registration failed';
      setError(formattedError);
      console.error("Signup failed:", err);
    }
  };

  return (
    <div style={{
      display: "flex",
      textAlign: "center",
      width: "33rem",
      flexDirection: "column",
      margin: "auto",
    }}>
      <h1 style={{ marginTop: "2rem" }}>Sign Up</h1>
      <Link to='/login'><p style={{ marginTop: "1rem" }}>Have an account?</p></Link>
      <form onSubmit={handleSubmit}>
        <div style={{
          display: "flex",
          flexDirection: "column",
          marginTop: "1rem"
        }}>
          <input
            style={{
              height: "3rem",
              marginTop: "1rem"
            }}
            type="text"
            placeholder='Username'
            value={username}
            onChange={handleUsernameChange}
          />
          <input
            style={{
              height: "3rem",
              marginTop: "1rem"
            }}
            type="email"
            placeholder='Email'
            value={email}
            onChange={handleEmailChange}
          />
          <input
            style={{
              height: "3rem",
              marginTop: "1rem"
            }}
            type='password'
            placeholder='Password'
            value={password}
            onChange={handlePasswordChange}
          />
          <input
            style={{
              width: "6rem",
              height: "2rem",
              marginTop: "1rem",
              alignSelf: "center",
              marginLeft:"27rem"
            }}
            type='submit'
            value='Sign Up'
          />
          {error && <p style={{ color: "red", marginTop: "1rem" }}>{error}</p>}
        </div>
      </form>
    </div>
  );
}
