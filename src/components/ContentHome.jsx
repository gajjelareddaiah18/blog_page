import React from 'react';

const ContentHome = ({ article }) => {
  return (
    <div className='content-home' style={{
      textAlign: "center",
      height: "auto",
      background: "rgb(92, 184, 92)",
      color: "white",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
      padding: "2rem"
    }}>
      {article ? (
        <div style={{ width: "68rem", textAlign: "left", background: "white", color: "black", padding: "2rem", borderRadius: "10px" }}>
          <div style={{ display: "flex", alignItems: "center" }}>
            <img style={{ borderRadius: "50%", marginRight: "1rem" }} src={article.image} alt={article.author} />
            <p>{article.author}</p>
          </div>
          <h2>{article.title}</h2>
          <p>{article.body}</p>
        </div>
      ) : (
        <div>
          <h1 style={{ fontSize: "60px" }}>conduit</h1>
          <p style={{ fontSize: "29px", opacity: "0.8" }}>A place to share your knowledge.</p>
        </div>
      )}
    </div>
  );
};

export default ContentHome;
