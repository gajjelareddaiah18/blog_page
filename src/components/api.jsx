
import axios from "axios";

export const fetchArticles = async () => {
  try {
    const response = await axios.get(
      "https://conduit.productionready.io/api/articles"
    );
    console.log(response);
    return response.data.articles.map(article => ({
      title: article.title,
      author: article.author.username,
      image: article.author.image,
      body:article.body,
      favoritesCount:article.favoritesCount
    }));
  } catch (error) {
    throw error;
  }
};
