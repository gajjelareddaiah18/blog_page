
import SingUp from "./components/SingUp"
import SignIn from "./components/SignIn";
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'; 
import ContentHome from "./components/ContentHome";
import Appbar from "./components/appbar";
import  ArticleData  from "./components/articaldata";


function App() {
  return (
    <>
      <BrowserRouter>
      <Appbar/>
        <Routes>
          <Route path="/" element={< ContentHome/>}></Route>
          <Route path="/home" element={<ArticleData/>}></Route>
          <Route path='/register' element={<SingUp/>} /> 
          <Route path='/login' element={<SignIn/>} /> 

        </Routes>
      </BrowserRouter>
   
    </>
  );
}

export default App;